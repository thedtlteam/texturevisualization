//Maeda Hanafi
//Webparser that reads from sanad from http://sunnah.com/ramadan

var webparser = function(indb, inpython){
	var db = indb;
	var pythonrunner = inpython;
	
	var request = require('request');
	var cheerio = require('cheerio');
	var jsdom = require('jsdom');
	var fs=require('fs');

	var url = 'http://sunnah.com/ramadan';
	
	var clientScript =''; //'var x = "http\\u00253A\\u00252F\\u00252Fexample.com";'
					 

	var htmlStub = '<head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" /></head>\n'
				 + '<script src="javascripts/libs/jquery-1.7.2.min.js"></script>\n'
				 + '<script> function displayArabic(x){\n' //convert the unicode to arabic characters
				 + 'var r = /\\u([\\d\\w]{4})/gi;\n'
				 + 'x = x.replace(r, function (match, grp) {\n'
				 + 'return String.fromCharCode(parseInt(grp, 16)); } );\n'
				 + 'x = unescape(x);\n'
				 + 'console.log(x);\n'
				 + 'return x;'
				 + '}</script>\n'
				   // + '<body><div id="container"></div> <pre id="output" ></pre> </body>\n';

 
	function init(){
		reader();
	}
	//Reads sanad data from the given link
	function reader(){
		
		request(url, function(err, resp, body) {
			if (err){
				console.log( err);
			}else{
				$ = cheerio.load(body);
				//console.log(body);

				$('.AllHadith .actualHadithContainer').each(function(hadith) {
					$(this).find('.englishcontainer').each(function() {
						//console.log("english:"+ $(this).text());
						var english = $(this).text();
					});
					$(this).find('.arabic_hadith_full.arabic').each(function() {
						//console.log("arabic full:"+ $(this).text());
						var arabicfull = $(this).text();
						var sanad = $(this).find('.arabic_sanad.arabic').first().text()
						console.log("sanad:"+sanad);
						//Extract names
						extractName(sanad);
						clientScript = clientScript +"console.log(\""+sanad+"\");\n"
						//arrayNames.forEach(function(name){ clientScript = clientScript+" \n console.log(\""+name+"\");\n" });
 						
					});
					
				});
				 
				renderpage();
				
			}
		});
		
	}
	
	//Returns an array of names given the sanad by running NLTK 
	function extractName(sanadtext){
		//Execute nltk
		//sanadparser.py accepts one argument, which is the sanad text
		console.log("Sanad in Unicode:"+toUnicode(sanadtext));
		
		var cmd = 'python public/python/sanadparser.py  '+toUnicode(sanadtext)+' ';
		console.log("Command to execute:"+cmd);
		
		pythonrunner.exec(cmd, function (error, stdout, stderr) {
			if (error) {
				console.log(error.stack);
				console.log('Error code: '+error.code);
				console.log('Signal received: '+error.signal);
			}else{
				//console.log('Child Process STDOUT: ');
				//console.log(stdout);
				if(stderr) console.log('Child Process STDERR: '+ stderr);
				
				renderNames(stdout);
				
				
			}
			
		});
		
		
	}
	
	function renderNames(names){
		htmlStub = htmlStub + names + '<br>';
				
		//Reload the displayparser.ejs
		request(url, function(err, resp, body) {
			if (err){
				console.log( err);
			}else{
				$ = cheerio.load(body);
				//clientScript = clientScript + "console.log(\"result from nltk:\" + \""+stdout.toString()+"\");\n"
				renderpage();
			}			
		});
	}
	
	//Converts a string to unicode
	function toUnicode(theString) {
		var unicodeString = '';
		for (var i=0; i < theString.length; i++) {
			var theUnicode = theString.charCodeAt(i).toString(16).toUpperCase();
			while (theUnicode.length < 4) {
				theUnicode = '0' + theUnicode;
			}
			theUnicode = '\\u' + theUnicode;
			unicodeString += theUnicode;
 		}
		return unicodeString;
	}

	function renderpage(){
		//Create page
		passToJsdom( htmlStub, clientScript);
	
	}
	
	function passToJsdom( htmlStub, clientScript){
		// pass the html stub to jsDom
		jsdom.env({ features : { QuerySelector : true }, html : htmlStub,
					done : function(errors, window) {
					
						var body = window.document.querySelector('body') 
						
						writeFile('views/displayparser.ejs',window, body, clientScript);
 					}
		})
	};
	
	function writeFile(filename, window, body, clientScript){
		// append the script to page's body
		d3.select(body)
			.append('script')
				.html(clientScript)
		// save result in an html file
		var fs = require('fs')
			, svgsrc = window.document.innerHTML
			
		fs.writeFile(filename, svgsrc, function(err) {
			if(err) {
				console.log('error saving document', err)
			} else {
				console.log('The file was saved, open '+filename+' (drop the views in the url) with to see the result')
			}
		})
	};	
	
	//Respond to GET request for /displayparser
 	function displayparser(req, res){
		res.render('displayparser');
		
	};

  	return  { 
		init:init,
 		reader: reader ,
		displayparser:displayparser
  	}
 };
 
 
//allow access for other files to access this
 exports.webparser = webparser;
 
  