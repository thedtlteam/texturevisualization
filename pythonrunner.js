//Maeda Hanafi
//NLTK is called from this file

var pythonrunner = function( ){
	var http = require('http');
	var childProcess = require('child_process'), ls;

 
	//Initializes the process by setting up exception catching
	function init(){
		/*process.on('uncaughtException', function ( err ) {
			console.log(err);
			console.error('An uncaughtException was found, the program will end.');
			//hopefully do some logging.
			process.exit(1);
		});*/
		  
	} 
	
	//Executes the command line stuff here (used to invoke nltk)
	function exec(cmd, callback){
		
		ls = childProcess.exec(cmd, callback);

		ls.on('exit', function (code) {
			console.log('Child process exited with exit code '+code);
		});
	}
	
  	return  { 
 		init: init ,
		exec: exec
  	}
 };
 
 
//allow access for other files to access this
exports.pythonrunner = pythonrunner;
  