
//Maeda Hanafi
//D3 everything here

/*===========================================================================
	RENDERING
============================================================================= */
var d3module = function(indb ){
	var d3 = require("d3"),
		jsdom = require('jsdom');
	//This module is used to solve the serialization of circular jsons
	var CircularJSON = require('circular-json');
	//Database
	var db = indb;
	
	var treeData = {array:[
		{
			"name": "Rasulullah",
			"parent": "null",
			"children": [
				{
					"name": "Umar ibn Khattab",
					"parent": "Rasulullah",
					"children": [
						{
							"name": "Al Qamah bin Waqas Laythi",
							"parent": "Umar ibn Khattab",
							"children": [
								{
									"name": "Akhbarani Muhammad ibn Ibrahim Ataymi",
									"parent": "Al Qamah bin Waqas Laythi",
									"children": [
										{
											"name": "Yahya bin Saeed Al Ansari",
											"parent": "Akhbarani Muhammad ibn Ibrahim Ataymi",
											"children": [
												{	
													"name": "Sufyan",
													"parent": "Yahya bin Saeed Al Ansari",
													"children":[
														{	
															"name": "Hamaydy Abdullah ibn Zubayr",
															"parent": "Sufyan"
															
														}
													]
												}
											]
										}
										
									]
								}
							]
						},
						{
							"name": "Someone",
							"parent": "Umar ibn Khattab"
						}
					]
				} 
			]
		}
	]};
	
	//Pre-rendering done here
	var init = function (){
		//render();
		renderhadithdata();
	}
	
	 //Pre-rendering example
	var render = function( ){
		var htmlStub = '<div id="dataviz-container"></div><script src="javascripts/d3.min.js"></script>' // html file skull with a container div for the d3 dataviz
 				+ '<script src="http://d3js.org/topojson.v1.min.js"></script>'
				+ '<script src="javascripts/datamaps.world.min.js"></script>'
				+ '<div id="container" style="position: relative; width: 500px; height: 300px;"></div>'
				+ '<script>'
				+ 'var map = new Datamap({element: document.getElementById("container")});'
				+ '</script>';
		// pass the html stub to jsDom
		jsdom.env({ features : { QuerySelector : true }, html : htmlStub,
					done : function(errors, window) {
						// process the html document, like if we were at client side
						var el = window.document.querySelector('#dataviz-container')
						, body = window.document.querySelector('body')
						// code to generate the dataviz and process the resulting html file to be added here
						
						var circleId = 'a2324'  // say, this value was dynamically retrieved from a database

						// append the svg to the selector
						d3.select(el)
							.append('svg:svg')
								.attr('width', 600).attr('height', 300)
								.append('circle')
									.attr('cx', 300).attr('cy', 150).attr('r', 30).attr('fill', '#26963c')
									.attr('id', circleId) // we assign the circle to an Id here

						 
						var vis = d3.select(el)
							.append("svg");
							
						var nodes = [{x: 30, y: 50},
									  {x: 50, y: 80},
									  {x: 90, y: 120}]
								
						var w = 900,
							 h = 400;
						 vis.attr("width", w)
							.attr("height", h);
							
						vis.text("Our Graph")
							.select("#dataviz-container")
		
						var node = vis.selectAll("circle.node")
							.data(nodes)
							.enter().append("g")
							.attr("class", "node")

						 node.append("svg:circle")
							.attr("cx", function(d) { return d.x; })
							.attr("cy", function(d) { return d.y; })
							.attr("r", "10px")
							.attr("fill", "black");

						 
						 vis.selectAll("circle.nodes")
							.data(nodes)
							.enter()
							.append("svg:circle")
							.attr("cx", function(d) { return d.x; })
							.attr("cy", function(d) { return d.y; })
							
						var links = [
							{source: nodes[0], target: nodes[1]},
							{source: nodes[2], target: nodes[1]}
						]
						
						vis.selectAll(".line")
						   .data(links)
						   .enter()
						   .append("line")
						   .attr("x1", function(d) { return d.source.x })
						   .attr("y1", function(d) { return d.source.y })
						   .attr("x2", function(d) { return d.target.x })
						   .attr("y2", function(d) { return d.target.y })
						   .style("stroke", "rgb(6,120,155)");

						// write the client-side script manipulating the circle
						var clientScript = "d3.select('#" + circleId + "').transition().delay(1000).attr('fill', '#f9af26')"
						
						writeFile('views/displayrender.ejs', window, body, clientScript);
 					}
		});
		
 	};
	
	
	function renderhadithdata(){ 
		//Create page
		var htmlStub = '<script src="javascripts/libs/jquery-1.7.2.min.js"></script>\n'
				+ '<script src="javascripts/d3.min.js"></script>\n' 
				+ '<script src="javascripts/buildTree.js"></script>\n'
				+ '<link rel="stylesheet" type="text/css" href="stylesheets/buildTree.css">\n'
				//+ '<body><h4>	Geographical Location </h4><div class="verticaltext"><h4>	Timeline </h4></div>'
				+ '<body><div id="container"></div> </body>';
				
						
		//Grab Hadith data
 		db.getAllTree(function(err1,data1) { 
			//console.log(JSON.stringify(data))
			if(err1){ console.log(err1) }
			else{ 		
				//Grab Hadith data
				db.getRootNode(function(err2,data2) { 
					if(err2){ console.log(err2) }
					else{ 			
						var clientScript = "var treeData = "+JSON.stringify( (data1))+";\n"
										//+"var root = "+data2[0]+"\n"
										//+"console.log(treeData);\n"
										//+"drawTree("+JSON.stringify(CircularJSON.stringify(getRoot(data1)))+");\n"
										+"drawTree( );\n"
						passToJsdom( htmlStub, clientScript);
					}
				});			
				
			}
 		});
		
	}
	
	function getRoot(links) {
		var nodesByName = {};
		
 		
		// Create nodes for each unique source and target.
		links.forEach(function(link) {
			var parent = link.source = nodeByName(link.source),
			child = link.target = nodeByName(link.target);
			if (parent.children) parent.children.push(child);
			else parent.children = [child];
		});

		return links[0].source;
		
		function nodeByName(name) {
			return nodesByName[name] || (nodesByName[name] = {name: name});
		}
	}

	function passToJsdom( htmlStub, clientScript){
		// pass the html stub to jsDom
		jsdom.env({ features : { QuerySelector : true }, html : htmlStub,
					done : function(errors, window) {
					
						var body = window.document.querySelector('body') 
						
						writeFile('views/displayhadith.ejs',window, body, clientScript);
 					}
				})
	};
	
	function writeFile(filename, window, body, clientScript){
		// append the script to page's body
		d3.select(body)
			.append('script')
				.html(clientScript)
		// save result in an html file
		var fs = require('fs')
			, svgsrc = window.document.innerHTML
			
		fs.writeFile(filename, svgsrc, function(err) {
			if(err) {
				console.log('error saving document', err)
			} else {
				console.log('The file was saved, open '+filename+' (drop the views in the url) with to see the result')
			}
		})
	};	
	
	
	
 	return  { 
		init:init
   	}
};
exports.d3module = d3module;
 

/*===========================================================================
	Initializing the routes for d3
============================================================================= */
//Respond to GET request for /circle
exports.circle =  function circle(req, res){	

};

//Respond to GET request for /displayrender
exports.displayrender = function displayrender(req, res){
	res.render('displayrender');
	
};
//Respond to GET request for /displayhadith
exports.displayhadith = function displayhadith(req, res){
	res.render('displayhadith');
}


