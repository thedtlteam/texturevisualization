//Maeda Hanafi
//Database functions

var database = function(/*inconnection*/ ){

	//var connection = inconnection;
	
	// To http://localhost:7474/db/data
	var dbLocal = require("seraph")();
	
	function cypher(query,params,cb) {
		console.log("Querying:"+query);
		dbLocal.query(query, {}, function(err, result) {
			if (err){ console.log(err);
			}else{ 
				console.log("Successful query!");//console.log(result);
				cb(err, result);
			
			}
		});
	}
 
/*===========================================================================
	NARRATOR node queries
============================================================================= */
	//Returns true or false 
	function isNarratorExist(name){
		var query = "match (person:PERSON {name:"+name+"}) return person"
		var params={ };
		var cb=function(err,data) { console.log(JSON.stringify(data)) }

		cypher(query,params,cb )
		//Must return true or false if there does exists one
	};
	
	//Creates a person who is a narrator
	function createNarrator(name){
		var query = "create (person:PERSON {name:"+name+"})"
		var params={ };
		var cb=function(err,data) { console.log(JSON.stringify(data)) }

		cypher(query,params,cb )
	};
	
	//Ensures the existence of all narrators in given array
	function ensureNarratorExistence(narrators){
		for(i =0; i<narrators.length; i++){
			if(!isNarratorExist(name)){
				//Create the narrator
				createNarrator(name);
			} 
		}
	}
/*===========================================================================
	NARRATED_TO relationship queries
============================================================================= */
	//Creates a unique relationship NARRATED_TO relationship between two given narrators
	function createRelationship(toNarrator, fromNarrator){
		//But first ensure that everyone in the chain already exists in the db
		ensureNarratorExistence([toNarrator, fromNarrator]);
		
		var query = "MATCH (from:PERSON),(to:PERSON) where to.name="+toNarrator+" and from.name="+fromNarrator+" create unique (from)-[:NARRATED_TO {status:\"Authentic\"}]->(to)"
		var params={ };
		var cb=function(err,data) { console.log(JSON.stringify(data)) }

		cypher(query,params,cb )
	}
	
	//Deletes relationship NARRATED_TO between two given narrators
	function deleteRelationship(toNarrator, fromNarrator){
		var query = "MATCH (from:PERSON),(to:PERSON) where to.name="+toNarrator+" and from.name="+fromNarrator+" create (from)-[:NARRATED_TO]->(to)"
		var params={ };
		var cb=function(err,data) { console.log(JSON.stringify(data)) }

		cypher(query,params,cb )
	
	}

/*===========================================================================
	Chain of narrators queries
============================================================================= */
	
	//Creates a path for an array of narrators
	//The first narrator is the one that appears first in the hadith
	function createPathChain(narrators){
		//But first ensure that everyone in the chain already exists in the db
		ensureNarratorExistence(narrators);
		
		//Create the query
		var querymatch = "match "; 
		var queryrelationship = "create ";
		for(i=0; i<narrators.length; i++){
			//Concatenate the first part of the query for matching the narrators
			querymatch = querymatch+"( node"+i+":PERSON{name:"+narrators[i]+"})";
			if(i!=narrators.length-1) {
				querymatch = querymatch + ", ";
			
				//Concatenate the part for creating the relationships
				queryrelationship = queryrelationship + "(node"+i+")<-[:NARRATED_TO]-(node"+(i+1)+")";
			}
		}
		
		var query = querymatch +" " +queryrelationship;
		var params={ };
		var cb=function(err,data) { console.log(JSON.stringify(data)) }

		cypher(query,params,cb )
	};
	
	//Delete a path between given an array of narrators
	//The first narrator is the one that appears first in the hadith
	function deletePathChain(narrators){
 		//match p =(from { name:"هِشَامٌ"  })-[:NARRATED_TO{ status:"Authentic" }]->(to { name: "مُسْلِمُ بْنُ إِبْرَاهِيمَ" })
		//delete p
		var querymatch = "match p="; 
		var querydel = "delete p";
		for(i=0; i<narrators.length-1; i++){
 			querymatch = querymatch+"(to { name:"+narrators[i+1]+"})<-[:NARRATED_TO]-(from {name:"+narrators[i]+"})";
			 
		}
		
		var query = querymatch +" " +querydel;
		var params={ };
		var cb=function(err,data) { console.log(JSON.stringify(data)) }

		cypher(query,params,cb )
	
	}
/*===========================================================================
	Hadith node queries
============================================================================= */
	//Creates a Hadith node
	//reference: Sunan an-Nasa\'i 2106
	//inbookref: Book 22, Hadith 17
	//english: full text of escaped English Hadith
	//arabic: full text of escaped arabic Hadith
	function createHadith(reference, inbookref, english, arabic, callback){
		var query = "create (n:HADITH{reference:"+ reference +", inbookref:"+ inbookref +
					",english:"+english+", arabic:"+arabic+"})";
		var params={  };
		var cb=callback; 

		cypher(query,params,cb )		
	}
	
	function modifyHadith(){
	
	}
/*===========================================================================
	Tree Drawing queries
============================================================================= */
function getAllTree(callback){
		var query = "match (n)<-[rel:NARRATED_TO]-(parent) return rel, n.name as name, parent.name as parent"
		var params={ };
		var cb=callback; 

		cypher(query,params,cb )
	};
	
 	function getRootNode(callback){
		var query = "START n=node(*) "+
					"MATCH (n)<-[:NARRATED_TO]-(root)"+
					"WHERE NOT (root)<-[:NARRATED_TO]-() "+
					"RETURN DISTINCT root;"
		var params={  };
		var cb=callback; 

		cypher(query,params,cb )			
					
	};
/*===========================================================================
	General queries
============================================================================= */
	function getAll(callback){
		var query = "match (n)<-[rel:NARRATED_TO]-(parent) return rel, n, parent"
		var params={ };
		var cb=callback; 

		cypher(query,params,cb )
	};
	
	
	
	
  	return  { 
 		getAll: getAll,
		getRootNode:getRootNode,
		getAllTree:getAllTree
  	}
 };
 
 
//allow access for other files to access this
 exports.database = database;