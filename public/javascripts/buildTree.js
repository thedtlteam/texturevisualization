// ************** Generate the tree diagram  *****************
var root;
function drawTree( ){
	 
	console.log((treeData));
 
	update( convertToHeirarchy()[0]);
 };
 function convertToHeirarchy(){
	 // create a name: node map
	var dataMap = treeData.reduce(function(map, node) {
		map[node.name] = node;
		return map;
	}, {});

	// create the tree array
	var tree = [];
	treeData.forEach(function(node) {
		// add to parent
		var parent = dataMap[node.parent];
		if (parent) {
			// create child array if it doesn't exist
			(parent.children || (parent.children = []))
				// add node to child array
				.push(node);
		} else {
			// parent is null or missing
			tree.push(node);
		}
	});
	console.log(tree);
	return tree;
 }


function nodeByName(name) {
	return nodesByName[name] || (nodesByName[name] = {name: name});
} 
 
//Draws the tree
function update(root) {
	
	var margin = {top: 20, right: 20, bottom: 20, left: 20},
	width = window.innerWidth - margin.right - margin.left,
	height = window.innerHeight - margin.top - margin.bottom;

	var spacing  = 50;
	 
	var i = 0;

	var tree = d3.layout.tree()
		.size([height, width]);

	var diagonal = d3.svg.diagonal()
		.projection(function(d) { return [d.x, d.y]; });

	var svg = d3.select("body").append("svg")
				.attr("width", width + margin.right + margin.left)
				.attr("height", height + margin.top + margin.bottom)
				.append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	
	// Compute the new tree layout.
	var nodes = tree.nodes(root).reverse(),
	links = tree.links(nodes);

	// Normalize for fixed-depth.
	nodes.forEach(function(d) { d.y = d.depth * spacing; });

	// Declare the nodesâ€¦
	var node = svg.selectAll("g.node")
					.data(nodes, function(d) { return d.id || (d.id = ++i); });

	// Enter the nodes.
	var nodeEnter = node.enter().append("g")
								.attr("class", "node")
								.attr("transform", function(d) { 
														return "translate(" + d.x + "," + d.y + ")"; });

	nodeEnter.append("circle")
			.attr("r", 10)
			.style("fill", "#fff");

	nodeEnter.append("text")
			.attr("x", function(d) { 
					return d.children || d._children ? -13 : 13; })
			.attr("dy", ".35em")
			.attr("text-anchor", function(d) { 
					return d.children || d._children ? "end" : "start"; })
			.text(function(d) { return d.name; })
			.style("fill-opacity", 1);

	// Declare the linksâ€¦
	var link = svg.selectAll("path.link")
					.data(links, function(d) { return d.target.id; });

	// Enter the links.
	link.enter().insert("path", "g")
		.attr("class", "link")
		.attr("d", diagonal);
 
	// Parse the date / time
	/*var	parseDate = d3.time.format("%d-%b-%y").parse;
 	// Set the ranges
	var	x = d3.time.scale().range([0, width]);
	var	y = d3.scale.linear().range([height, 0]);
	 
	// Define the axes
	var	xAxis = d3.svg.axis().scale(x)
		.orient("bottom").ticks(5);
	 
	var	yAxis = d3.svg.axis().scale(y)
		.orient("left").ticks(5);
	 
	// Define the line
	var	valueline = d3.svg.line()
		.x(function(d) { return x(d.date); })
		.y(function(d) { return y(d.close); });
		

	// Add the X Axis
	svg.append("g")		
		.attr("class", "x axis")
		.call(xAxis) ;
 
	// Add the Y Axis
	svg.append("g")		
		.attr("class", "y axis")
		.call(yAxis);
	
	//Add axes labels
	svg.append("text")
		.attr("class", "x label")
		.attr("text-anchor", "end")
		.attr("x", width/2)
		.attr("y", margin.top/2)
		.text("Geographical Location");
		
	svg.append("text")
		.attr("class", "y label")
		.attr("text-anchor", "end")
 		.attr("y", margin.right/2)
		.attr("dy", ".75em")
		.attr("transform", "rotate(-90)")
		.text( "Timeline");*/
	 
}
	 

	 
 