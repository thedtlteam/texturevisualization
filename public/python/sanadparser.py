#!/c/Python27/python
# -*- coding: utf8 -*-

#print "Content-type: text/html\n\n"

import sys

import nltk
import nltk.data
from nltk.stem.isri import ISRIStemmer

#Contains all the stems of the words that are not names (thus covering variations of Akhbarna, etc)
notNameStemmed = [u'\u062e\u0628\u0631', u'\u062d\u062f\u062b', u'\u0639\u0646', u'\u0642\u0627\u0644']
#Contains the phrases Radiyallahan and Radiyalahanhuma
notNamePhrases = [u'\u0631\u0636\u0649 \u0627\u0644\u0644\u0647 \u0639\u0646\u0647', u'\u0631\u0636\u0649 \u0627\u0644\u0644\u0647 \u0639\u0646\u0647\u0645\u0627']

#Return true or false if the word appears  in any of the above arrays
def isName(word):
  	stemresult = stem("".join([unichr(int(x, 16)) for x in word.split("\\u") if x !=""]))
 	if stemresult in  notNameStemmed: 
		return False
	else:
		return 1

#Given a unicode string of format for example u'u\3242u\3432u\2432',
#return the stem in the format u'u\3242u\3432u\2432'
def stem(word):	
	st = ISRIStemmer()
	result = st.stem(word)
 	return result
 


#Start of program******************************
	
#Display the sentence we are about to parse

sentence = str(sys.argv[1]) 

print('sanad to parse:<br>')
sys.stdout.write('<script>document.write(displayArabic(\''+unicode(sentence)+'\'))</script>')
sys.stdout.write('<br>')

sentence = unicode(sentence, 'utf-8')
#Important! Convert unicode space to actual character space in order for token to work
sentence = sentence.replace('\u0020', ' ')

#Split by the sentence by the Arabic comma
commasplit = sentence.split('\u060C')
 

for commasection in commasplit:
	name = ''
	names = []
	index = 0 
	prevNotName = False
	
	#Get rid of phrases that are exactly in array notNamePhrases
	for phrase in notNamePhrases:
		commasection = commasection.replace(phrase, '')
 	
	#Get every word in commasection
	tokens = nltk.word_tokenize(commasection)
	 
	#Concatenate the words when a name is found
	for word in tokens:
		 
		if isName(word):
			#Add a new name if the word before was identified as a <Not a name>
			if prevNotName == True:
				index = index + 1;
			name  = name  + ' ' + word
			prevNotName = False
		else:
			#Add the concatenated name to the names list if the word before was identified as a <Name>
			if prevNotName == False:
				names.append(name)
				name = ''
			prevNotName = True
		
	names.append(name)
 			
	for name in names:
		sys.stdout.write('<script>document.write(displayArabic(\''+unicode(name)+'\'))</script>')
		sys.stdout.write('<br>')
 

	
	
