#!/c/Python27/python
# -*- coding: utf8 -*-

print "Content-type: text/html\n\n"

import nltk
import nltk.data
sentence = """حَدَّثَنَا آدَمُ بْنُ أَبِي إِيَاسٍ، حَدَّثَنَا شُعْبَةُ، حَدَّثَنَا عَبْدُ الْعَزِيزِ بْنُ صُهَيْبٍ"""
sentence = unicode(sentence, 'utf-8')
tokens = nltk.word_tokenize(sentence)
print(tokens)

tagged = nltk.pos_tag(tokens)
print(tagged[0:6])